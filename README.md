# MonkCLI

## SetUp Example

```javascript
const path = require('path')

global.rootRequire = dep => require(path.join(__dirname, '../app/', dep))
global.__environment = process.env.NODE_ENV || 'development'
global.__basePath = path.join(__dirname, '../app/')
global.__dataFolder = path.join(process.cwd(), 'data/')
global.__config = rootRequire('config/')

const CLIservice = require('@krknet/monkcli')
const Connection = require('@krknet/mongo-connector')
global.__db = new Connection({ url: global.__config.mongo, doLog: false })

const service = new CLIservice({
  dbConnection: global.__db,
  models: [
    rootRequire('models/access'),
    rootRequire('models/accounts')
  ]
})

service.query(process.argv)
```

## MonkModel Schema Additions

### resolver
`resolver` allows for easier value lookup and selection.

Example:
```javascript
async function instanceResolver ({ results, eq, initial }) {
  const instances = await rootRequire('models/instances').get()
  const { id } = await eq.prompt({
    type: 'select',
    name: 'instance',
    message: 'Instanz',
    initial,
    choices: instances.map(d => ({ name: d.data._id, message: `${d.title}` }))
  })

  return id
}
```

### options
`options` allows for ENUM style value selection.

Example:
```javascript
{
  mode: {
    type: String,
    title: 'Zugangs-Modus',
    require: false,
    default: 'closed',
    options: ['open', 'account', 'admin', 'closed']
  }
}
```

### setter
`setter` model function name to set field.

Example:
```javascript
{
  password: {
    type: String,
    title: 'Passwort',
    required: false,
    setter: 'setPassword'
  }
}
```
