const Form = require('../form')
const Profiler = require('@krknet/profiler')

async function operation (Entity) {
  const form = new Form(Entity)
  const input = await form.query()

  const excludedKeys = form.settersFields.map(d => d.key)
  const cleanedInput = Object.fromEntries(Object.entries(input).filter(([key, def]) => !excludedKeys.includes(key)))

  const entry = new Entity(cleanedInput)

  for (const key of excludedKeys) {
    await entry[form.schema[key].setter](input[key])
  }

  await entry.save()
  Profiler.success(`Created Entry: ${entry.title || entry.data._id}`)
}

module.exports = operation
