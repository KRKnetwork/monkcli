const Profiler = require('@krknet/profiler')
const Table = require('cli-table3')

async function operation (Entity) {
  const profiler = new Profiler('Listing Entries').start()
  const fields = [
    ['_id'],
    ...Object.entries(Entity._modelSchema),
    ...Object.entries(Entity._baseSchema.schema).filter(([key]) => key !== '_id')
  ]
    .map(([key, def]) => ({ key: key, title: (def || {}).title || key, def }))

  const table = new Table({
    head: fields.map(d => d.title),
    style: { compact: true, head: ['cyan', 'bold'] }
  })

  const entries = await Entity.get()

  if (entries.length === 0) return profiler.fail('No entries found!')

  for (const entry of entries) table.push(fields.map(({ key, def }) => formatField(def, entry.data[key])))

  profiler.succeed()
  console.log(table.toString())
}

module.exports = operation

function formatField (def = {}, value) {
  if (typeof def.formatter === 'function') return def.formatter(value)
  if (value instanceof Date) return value.toISOString().replace('T', ' ').split('.')[0]
  if (Array.isArray(value)) return JSON.stringify(value)
  return value
}
