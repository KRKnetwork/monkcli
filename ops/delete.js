const Profiler = require('@krknet/profiler')
const eq = require('enquirer')

async function operation (Entity) {
  Profiler.warn(`Deleting ${Entity.name}`)

  const entries = await Entity.get()
  if (entries.length === 0) return Profiler.warn('No entries found!')

  const choices = entries.map(d => ({ name: d.data._id, message: d.title || d.data._id }))
  const prompt = new eq.Select({ message: 'Pick an entry', choices})
  const id = await prompt.run()

  const entry = await Entity.get(id)
  await entry.delete()

  Profiler.success(`Deleted Entry: ${entry.title || entry.data._id}`)
}

module.exports = operation
