const Profiler = require('@krknet/profiler')
const Form = require('../form')
const eq = require('enquirer')

async function operation (Entity) {
  const entries = await Entity.get()
  if (entries.length === 0) return Profiler.warn('No entries found!')

  const choices = entries.map(d => ({ name: d.data._id, message: d.title || d.data._id }))
  const prompt = new eq.Select({ message: 'Pick an entry', choices })
  const id = await prompt.run()

  const entry = await Entity.get(id)

  const form = new Form(Entity)
  const input = await form.query(entry.data)

  const excludedKeys = form.settersFields.map(d => d.key)
  const cleanedInput = Object.fromEntries(Object.entries(input).filter(([key, def]) => !excludedKeys.includes(key)))

  entry.patch(cleanedInput)

  for (const key of excludedKeys) {
    if (input[key] !== undefined) await entry[form.schema[key].setter](input[key])
  }

  await entry.save()
  Profiler.success(`Updated Entry: ${entry.title || entry.data._id}`)
}

module.exports = operation
